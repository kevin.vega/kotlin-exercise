package com.example.kotlin_exercise.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin_exercise.R
import com.example.kotlin_exercise.data.Project
import com.example.kotlin_exercise.data.ProjectViewModel
import com.example.kotlin_exercise.data.RecyclerAdapter

class MainFragment : Fragment() {

    private val projects = listOf(
        Project("Kamisato Ayaka", "Daughter of the Yashiro Commission's Kamisato Clan. Dignified and elegant, as well as wise and strong.", R.drawable.ayaka),
        Project("Zhongli", "A mysterious expert contracted by the Wangsheng Funeral Parlor. Extremely knowledgeable in all things.", R.drawable.zhongli),
        Project("Raiden Shogun", "Her Excellency, the Almighty Narukami Ogosho, who promised the people of Inazuma an unchanging Eternity.", R.drawable.raiden),
        Project("Tartaglia", "No. 11 of The Harbingers, also known as Childe. His name is highly feared on the battlefield.", R.drawable.childe),
        Project("Hutao", "The 77th Director of the Wangsheng Funeral Parlor. She took over the business at a rather young age.", R.drawable.hutao),
        Project("Ganyu", "The secretary at Yuehai Pavilion. The blood of the qilin, an illuminated beast, flows within her veins.", R.drawable.ganyu)
    )

    private lateinit var viewModel: ProjectViewModel
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recyclerView)
        viewModel = ViewModelProvider(this)[ProjectViewModel::class.java]

        for (project in projects) {
            viewModel.addProject(project)
            observe()
        }

        (activity as AppCompatActivity).supportActionBar?.title = "Main"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun observe() {
        viewModel.getProjects().observe(viewLifecycleOwner, {
            recyclerView.adapter = RecyclerAdapter(it)
        })
    }
}