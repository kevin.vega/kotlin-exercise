package com.example.kotlin_exercise.data

import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData

class ProjectViewModel: ViewModel() {

    private val projectList = mutableListOf<Project>()
    private val projectsLiveData = MutableLiveData<MutableList<Project>>()

    fun addProject(project: Project) {
        projectList.add(project)
        projectsLiveData.value = projectList
    }

    fun getProjects(): MutableLiveData<MutableList<Project>> {
        return projectsLiveData
    }
}