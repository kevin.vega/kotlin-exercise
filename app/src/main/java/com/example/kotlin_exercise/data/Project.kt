package com.example.kotlin_exercise.data

import java.io.Serializable

data class Project(private val name: String, private val description: String, private val image: Int): Serializable {
    val projectImage: Int get() = image
    val projectDescription: String get() = description
    val projectName: String get() = name
}